#!/bin/bash
./wait-for-it.sh db:5432 -t 60
./wait-for-it.sh keycloak:8080 -t 60
java -jar currency-exchanger-0.0.1-SNAPSHOT.jar
