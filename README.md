# Currency Exchanger App

## Build & Run
`mvn clean package`

`docker-compose up --build`

## Monitoring

#### Prometheus

[Prometheus](http://localhost:9090)

[Prometheus targets](http://localhost:9090/targets)

#### Grafana

[Grafana Dashboard](http://localhost:3000/d/spring_boot_21/spring-boot-3x-statistics)

## Keycloak Admin Panel

[Keycloak admin panel](http://localhost:8090)

## Tech Stack
- Java 17
- Spring Boot 3.1.2
- Docker
- Docker Compose
- PostgresSQL
- Prometheus
- Grafana
- Keycloak

## How to use? How to test?

[Try swagger](http://localhost:8080/swagger-ui/index.html)


But in swagger you dont have configured keycloak endpoint. 

Additionally, I attached postman [Postman collection](postman/Currency%20Exchanger.postman_collection.json) with [environment](postman/Docker%20Env.postman_environment.json).
I configured that, postman automatically save accessToken from keycloak into environment.
If you call Create User Endpoint (unprotected) - id and password from response will save to postman environment also - you need this data to get keycloak token.
