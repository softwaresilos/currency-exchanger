FROM openjdk:17-jdk-slim-buster
MAINTAINER jakub@malinowski.net.pl

COPY target/currency-exchanger-0.0.1-SNAPSHOT.jar currency-exchanger-0.0.1-SNAPSHOT.jar
COPY docker/start.sh start.sh
COPY docker/wait-for-it.sh wait-for-it.sh
EXPOSE 8080

RUN chmod +x start.sh
RUN chmod +x wait-for-it.sh
ENTRYPOINT ["./start.sh"]
