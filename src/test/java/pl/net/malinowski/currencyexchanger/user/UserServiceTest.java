package pl.net.malinowski.currencyexchanger.user;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.keycloak.admin.client.Keycloak;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pl.net.malinowski.currencyexchanger.CurrencyExchangerApplication;
import pl.net.malinowski.currencyexchanger.account.AccountEntity;
import pl.net.malinowski.currencyexchanger.account.AccountRepository;
import pl.net.malinowski.currencyexchanger.account.SupportedCurrency;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static pl.net.malinowski.currencyexchanger.utils.AccountTestUtils.getAccountByCurrency;

@ExtendWith(SpringExtension.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = CurrencyExchangerApplication.class)
class UserServiceTest {

    @MockBean
    private Keycloak keycloak;

    @Autowired
    private UserService userService;

    @Autowired
    private AccountRepository accountRepository;

    @MockBean
    private JwtDecoder jwtDecoder;

    @Test
    void shouldCreateUserWithAccounts() {
        //given
        CreateUserRequestDto request = CreateUserRequestDto.builder()
                .firstName("Jan")
                .lastName("Kowalski")
                .initialPlnBalance(new BigDecimal("100.00"))
                .build();

        //when
        CreateUserResponseDto result = userService.create(request);
        UserEntity user = userService.findUserById(result.id());
        List<AccountEntity> accounts = accountRepository.findByUser(user);

        //then
        assertNotNull(result);
        assertNotNull(result.id());
        assertEquals(request.firstName(), user.getFirstName());
        assertEquals(request.lastName(), user.getLastName());
        AccountEntity plnAccount = getAccountByCurrency(accounts, SupportedCurrency.PLN);
        assertNotNull(plnAccount);
        assertEquals(request.initialPlnBalance(), plnAccount.getBalance());
        AccountEntity usdAccount = getAccountByCurrency(accounts, SupportedCurrency.USD);
        assertNotNull(usdAccount);
    }
}
