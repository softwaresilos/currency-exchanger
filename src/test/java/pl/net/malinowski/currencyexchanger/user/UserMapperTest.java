package pl.net.malinowski.currencyexchanger.user;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class UserMapperTest {

    private final UserMapper mapper = new UserMapperImpl();

    @Test
    void shouldMapToCreateResponseDto() {
        //given
        UserEntity user = UserEntity.builder()
                .id(UUID.randomUUID())
                .build();

        //when
        CreateUserResponseDto result = mapper.mapToCreateResponseDto(user);

        //then
        assertNotNull(result);
        assertEquals(user.getId().toString(), result.id());
    }

    @Test
    void shouldMapToEntity() {
        //given
        CreateUserRequestDto userDto = CreateUserRequestDto.builder()
                .firstName("Jan")
                .lastName("Kowalski")
                .initialPlnBalance(new BigDecimal("100.00"))
                .build();

        //when
        UserEntity result = mapper.mapToEntity(userDto);

        //then
        assertNotNull(result);
        assertEquals(userDto.firstName(), result.getFirstName());
        assertEquals(userDto.lastName(), result.getLastName());
    }
}
