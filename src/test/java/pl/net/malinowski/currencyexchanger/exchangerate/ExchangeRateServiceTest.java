package pl.net.malinowski.currencyexchanger.exchangerate;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.net.malinowski.currencyexchanger.account.SupportedCurrency;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ExchangeRateServiceTest {


    @Mock
    private NbpExchangeRateApiClient nbpExchangeRateApiClient;

    @InjectMocks
    private ExchangeRateService exchangeRateService;

    @Test
    void shouldReturnExchangeRate() {
        //given
        NbpApiRate rate = NbpApiRate.builder()
                .no("1234")
                .effectiveDate(LocalDate.now())
                .bid(new BigDecimal("4.1022"))
                .ask(new BigDecimal("4.2212"))
                .build();
        NbpApiResponse apiResponse = NbpApiResponse.builder()
                .code(SupportedCurrency.USD.name())
                .rates(Collections.singletonList(rate))
                .build();
        when(nbpExchangeRateApiClient.getExchangeRate(SupportedCurrency.USD))
                .thenReturn(apiResponse);

        //when
        ExchangeRateDto result = exchangeRateService.fetchActualRateInPln(SupportedCurrency.USD);

        //then
        assertNotNull(result);
        assertEquals(rate.bid(), result.buyingRate());
        assertEquals(rate.ask(), result.sellingRate());
    }
}
