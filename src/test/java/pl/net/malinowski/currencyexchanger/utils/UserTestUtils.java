package pl.net.malinowski.currencyexchanger.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.net.malinowski.currencyexchanger.user.UserEntity;

import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserTestUtils {

    public static UserEntity createUser(String givenUserId) {
        return UserEntity.builder()
                .id(UUID.fromString(givenUserId))
                .firstName("Jan")
                .lastName("Kowalski")
                .build();
    }
}
