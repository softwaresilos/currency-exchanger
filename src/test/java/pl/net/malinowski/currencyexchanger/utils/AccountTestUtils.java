package pl.net.malinowski.currencyexchanger.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.net.malinowski.currencyexchanger.account.AccountEntity;
import pl.net.malinowski.currencyexchanger.account.SupportedCurrency;

import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AccountTestUtils {

    public static AccountEntity getAccountByCurrency(List<AccountEntity> accounts, SupportedCurrency currency) {
        return accounts.stream()
                .filter(account -> currency == account.getCurrency())
                .findFirst()
                .orElse(null);
    }
}
