package pl.net.malinowski.currencyexchanger.account.operation;

import org.junit.jupiter.api.Test;
import pl.net.malinowski.currencyexchanger.account.AccountDetailsDto;
import pl.net.malinowski.currencyexchanger.account.AccountEntity;
import pl.net.malinowski.currencyexchanger.account.SupportedCurrency;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class AccountOperationMapperTest {

    private final AccountOperationMapper mapper = new AccountOperationMapperImpl();

    @Test
    void shouldMap() {
        //given
        String givenId = UUID.randomUUID().toString();
        AccountEntity plnAccount = AccountEntity.builder()
                .balance(BigDecimal.ONE)
                .id(1L)
                .currency(SupportedCurrency.PLN)
                .build();

        //when
        AccountOperationResultResponseDto result = mapper.map(givenId, Collections.singletonList(plnAccount));

        //then
        assertNotNull(result);
        assertEquals(givenId, result.id());
        AccountDetailsDto mappedAccount = result.accounts().get(0);
        assertEquals(plnAccount.getCurrency(), mappedAccount.currency());
        assertEquals(plnAccount.getBalance(), mappedAccount.balance());
    }
}
