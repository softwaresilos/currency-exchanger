package pl.net.malinowski.currencyexchanger.account.operation;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.net.malinowski.currencyexchanger.account.AccountEntity;
import pl.net.malinowski.currencyexchanger.account.AccountRepository;
import pl.net.malinowski.currencyexchanger.account.NotEnoughFundsOnSourceAccountException;
import pl.net.malinowski.currencyexchanger.account.SupportedCurrency;
import pl.net.malinowski.currencyexchanger.account.transaction.CurrencyTransactionEntity;
import pl.net.malinowski.currencyexchanger.account.transaction.CurrencyTransactionService;
import pl.net.malinowski.currencyexchanger.exchangerate.ExchangeRateDto;
import pl.net.malinowski.currencyexchanger.exchangerate.ExchangeRateService;
import pl.net.malinowski.currencyexchanger.user.UserEntity;
import pl.net.malinowski.currencyexchanger.user.UserService;
import pl.net.malinowski.currencyexchanger.utils.UserTestUtils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountOperationServiceTest {

    @Mock
    private ExchangeRateService exchangeRateService;

    @Mock
    private UserService userService;

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private CurrencyTransactionService currencyTransactionService;

    @Mock
    private AccountOperationMapper accountOperationMapper;

    @InjectMocks
    private AccountOperationService accountOperationService;

    @Test
    void shouldMakeExchangePlnToUsd() {
        //given
        String givenUserId = UUID.randomUUID().toString();
        AccountOperationRequestDto operationDto = AccountOperationRequestDto.builder()
                .operationAmount(new BigDecimal("50.00"))
                .sourceCurrency(SupportedCurrency.PLN)
                .destinationCurrency(SupportedCurrency.USD)
                .build();

        UserEntity user = UserTestUtils.createUser(givenUserId);

        AccountEntity plnAccount = createAccount(SupportedCurrency.PLN)
                .toBuilder()
                .user(user)
                .balance(new BigDecimal("100.00"))
                .build();
        AccountEntity usdAccount = createAccount(SupportedCurrency.USD)
                .toBuilder()
                .user(user)
                .build();
        List<AccountEntity> accounts = Arrays.asList(plnAccount, usdAccount);


        when(userService.findUserById(givenUserId))
                .thenReturn(user);
        when(accountRepository.findByUser(user))
                .thenReturn(accounts);
        when(exchangeRateService.fetchActualRateInPln(operationDto.destinationCurrency()))
                .thenReturn(getExchangeRate());
        doNothing()
                .when(currencyTransactionService)
                .createTransactionEntry(any(CurrencyTransactionEntity.class));

        BigDecimal expectedPlnBalance = new BigDecimal("50.00");
        BigDecimal expectedUsdBalance = new BigDecimal("12.14");

        //when
        accountOperationService.exchange(givenUserId, operationDto);

        //then
        assertEquals(expectedPlnBalance, plnAccount.getBalance());
        assertEquals(expectedUsdBalance, usdAccount.getBalance());
    }

    @Test
    void shouldMakeExchangeUsdToPln() {
        //given
        String givenUserId = UUID.randomUUID().toString();
        AccountOperationRequestDto operationDto = AccountOperationRequestDto.builder()
                .operationAmount(new BigDecimal("10.00"))
                .sourceCurrency(SupportedCurrency.USD)
                .destinationCurrency(SupportedCurrency.PLN)
                .build();

        UserEntity user = UserTestUtils.createUser(givenUserId);

        AccountEntity plnAccount = createAccount(SupportedCurrency.PLN)
                .toBuilder()
                .user(user)
                .build();
        AccountEntity usdAccount = createAccount(SupportedCurrency.USD)
                .toBuilder()
                .balance(new BigDecimal("34.99"))
                .user(user)
                .build();
        List<AccountEntity> accounts = Arrays.asList(plnAccount, usdAccount);


        when(userService.findUserById(givenUserId))
                .thenReturn(user);
        when(accountRepository.findByUser(user))
                .thenReturn(accounts);
        when(exchangeRateService.fetchActualRateInPln(SupportedCurrency.USD))
                .thenReturn(getExchangeRate());
        doNothing()
                .when(currencyTransactionService)
                .createTransactionEntry(any(CurrencyTransactionEntity.class));

        BigDecimal expectedPlnBalance = new BigDecimal("42.21");
        BigDecimal expectedUsdBalance = new BigDecimal("24.99");

        //when
        accountOperationService.exchange(givenUserId, operationDto);

        //then
        assertEquals(expectedPlnBalance, plnAccount.getBalance());
        assertEquals(expectedUsdBalance, usdAccount.getBalance());
    }

    @Test
    void shouldFailWhenNoEnoughFundsOnAccount() {
        //given
        String givenUserId = UUID.randomUUID().toString();
        AccountOperationRequestDto operationDto = AccountOperationRequestDto.builder()
                .operationAmount(new BigDecimal("10.00"))
                .sourceCurrency(SupportedCurrency.USD)
                .destinationCurrency(SupportedCurrency.PLN)
                .build();

        UserEntity user = UserTestUtils.createUser(givenUserId);

        AccountEntity plnAccount = createAccount(SupportedCurrency.PLN)
                .toBuilder()
                .user(user)
                .build();
        AccountEntity usdAccount = createAccount(SupportedCurrency.USD)
                .toBuilder()
                .balance(BigDecimal.ZERO)
                .user(user)
                .build();
        List<AccountEntity> accounts = Arrays.asList(plnAccount, usdAccount);

        when(userService.findUserById(givenUserId))
                .thenReturn(user);
        when(accountRepository.findByUser(user))
                .thenReturn(accounts);

        //when
        assertThrows(NotEnoughFundsOnSourceAccountException.class,
                () -> accountOperationService.exchange(givenUserId, operationDto));
    }

    private AccountEntity createAccount(SupportedCurrency currency) {
        return AccountEntity.builder()
                .currency(currency)
                .balance(BigDecimal.ZERO)
                .build();
    }

    private ExchangeRateDto getExchangeRate() {
        return ExchangeRateDto.builder()
                .buyingRate(new BigDecimal("4.1182"))
                .sellingRate(new BigDecimal("4.2212"))
                .build();
    }
}
