package pl.net.malinowski.currencyexchanger.account.details;

import org.junit.jupiter.api.Test;
import pl.net.malinowski.currencyexchanger.account.AccountDetailsDto;
import pl.net.malinowski.currencyexchanger.account.AccountEntity;
import pl.net.malinowski.currencyexchanger.account.SupportedCurrency;
import pl.net.malinowski.currencyexchanger.user.UserEntity;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class AccountDetailsMapperTest {

    private final AccountDetailsMapper mapper = new AccountDetailsMapperImpl();

    @Test
    void shouldMapEntitiesToAccountsDetailsResponseDto() {
        //given
        UserEntity user = UserEntity.builder()
                .id(UUID.randomUUID())
                .firstName("Jan")
                .lastName("Kowalski")
                .build();

        AccountEntity plnAccount = AccountEntity.builder()
                .balance(new BigDecimal("100.00"))
                .currency(SupportedCurrency.PLN)
                .user(user)
                .id(1L)
                .build();

        AccountEntity usdAccount = AccountEntity.builder()
                .balance(new BigDecimal("10.20"))
                .currency(SupportedCurrency.USD)
                .user(user)
                .id(2L)
                .build();

        List<AccountEntity> accounts = Arrays.asList(plnAccount, usdAccount);

        //when
        AccountsDetailsResponseDto result = mapper.mapToAccountsDetailsResponseDto(user, accounts);

        //then
        assertNotNull(result);
        assertEquals(user.getId().toString(), result.id());
        assertEquals(user.getLastName(), result.lastName());
        assertEquals(user.getFirstName(), result.firstName());
        AccountDetailsDto plnAccountDto = getAccountByCurrency(result.accounts(), SupportedCurrency.PLN);
        assertEquals(plnAccount.getId(), plnAccountDto.id());
        assertEquals(plnAccount.getBalance(), plnAccountDto.balance());
        AccountDetailsDto usdAccountDto = getAccountByCurrency(result.accounts(), SupportedCurrency.USD);
        assertEquals(usdAccount.getId(), usdAccountDto.id());
        assertEquals(usdAccount.getBalance(), usdAccountDto.balance());
    }

    private AccountDetailsDto getAccountByCurrency(List<AccountDetailsDto> accounts, SupportedCurrency currency) {
        return accounts.stream()
                .filter(account -> currency == account.currency())
                .findFirst()
                .orElse(null);
    }
}
