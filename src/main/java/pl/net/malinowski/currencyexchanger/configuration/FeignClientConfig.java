package pl.net.malinowski.currencyexchanger.configuration;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@EnableFeignClients(basePackages = "pl.net.malinowski")
@Configuration
class FeignClientConfig {

}
