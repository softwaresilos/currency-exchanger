package pl.net.malinowski.currencyexchanger.configuration;

import lombok.RequiredArgsConstructor;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
class KeycloakAdminConfig {


    private final KeycloakAdminProperties keycloakAdminProperties;

    @Bean
    Keycloak keycloak() {
        return KeycloakBuilder.builder()
                .serverUrl(keycloakAdminProperties.getServerUrl())
                .clientId(keycloakAdminProperties.getClientId())
                .clientSecret(keycloakAdminProperties.getClientSecret())
                .realm(keycloakAdminProperties.getRealm())
                .grantType(OAuth2Constants.PASSWORD)
                .username(keycloakAdminProperties.getUsername())
                .password(keycloakAdminProperties.getPassword())
                .build();
    }
}
