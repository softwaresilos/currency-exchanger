package pl.net.malinowski.currencyexchanger.user;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.net.malinowski.currencyexchanger.account.AccountEntity;
import pl.net.malinowski.currencyexchanger.account.AccountService;

import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final AccountService accountService;
    private final UserMapper userMapper;

    CreateUserResponseDto create(CreateUserRequestDto userDto) {
        UserEntity user = userRepository.save(userMapper.mapToEntity(userDto));
        List<AccountEntity> accounts = accountService.createAccounts(user, userDto.initialPlnBalance());
        log.info("User with id {} created with {} accounts", user.getId(), accounts.size());
        return userMapper.mapToCreateResponseDto(user);
    }

    public UserEntity findUserById(String id) {
        return userRepository.findById(UUID.fromString(id))
                .orElseThrow(() -> new EntityNotFoundException(String.format("User with id %s not found", id)));
    }
}
