package pl.net.malinowski.currencyexchanger.user;

import lombok.Builder;

@Builder(toBuilder = true)
record CreateUserResponseDto(String id,
                             String password) {

}
