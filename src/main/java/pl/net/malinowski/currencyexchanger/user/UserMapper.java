package pl.net.malinowski.currencyexchanger.user;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import pl.net.malinowski.currencyexchanger.configuration.MapStructConfig;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, config = MapStructConfig.class)
interface UserMapper {

    @Mapping(target = "password", ignore = true)
    CreateUserResponseDto mapToCreateResponseDto(UserEntity user);

    @Mapping(target = "id", ignore = true)
    UserEntity mapToEntity(CreateUserRequestDto userDto);
}
