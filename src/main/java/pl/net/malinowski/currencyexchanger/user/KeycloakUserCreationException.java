package pl.net.malinowski.currencyexchanger.user;

public class KeycloakUserCreationException extends RuntimeException {

    public KeycloakUserCreationException() {
        super("Error while creating user in Keycloak");
    }
}
