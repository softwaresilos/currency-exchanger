package pl.net.malinowski.currencyexchanger.user;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
class UserController {

    private final UserKeycloakService userKeycloakService;

    @PostMapping
    CreateUserResponseDto createKc(@Valid @RequestBody CreateUserRequestDto user) {
        return userKeycloakService.createUser(user);
    }
}
