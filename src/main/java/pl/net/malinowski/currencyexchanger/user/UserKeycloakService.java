package pl.net.malinowski.currencyexchanger.user;

import jakarta.transaction.Transactional;
import jakarta.ws.rs.core.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.stereotype.Service;
import pl.net.malinowski.currencyexchanger.configuration.KeycloakAdminProperties;

import java.util.Collections;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
class UserKeycloakService {

    private final Keycloak keycloak;
    private final KeycloakAdminProperties keycloakAdminProperties;
    private final UserService userService;

    @Transactional
    public CreateUserResponseDto createUser(CreateUserRequestDto createUserDto) {
        CreateUserResponseDto userResponseDto = userService.create(createUserDto);
        UserRepresentation userRepresentation = buildKeycloakUserRepresentation(createUserDto, userResponseDto.id());
        RealmResource realmResource = keycloak.realm(keycloakAdminProperties.getRealm());
        UsersResource usersResource = realmResource.users();

        createUserInKeycloak(userRepresentation, realmResource, usersResource);
        return userResponseDto.toBuilder()
                .password(userRepresentation.getCredentials().get(0).getValue())
                .build();
    }

    private void createUserInKeycloak(UserRepresentation userRepresentation, RealmResource realmResource, UsersResource usersResource) {
        log.info("Creating user in keycloak with username {}", userRepresentation.getUsername());
        Response response = usersResource
                .create(userRepresentation);
        validateCreateKeycloakUserResponse(response);
        assignRoles(userRepresentation, realmResource, usersResource, response);
    }

    private void assignRoles(UserRepresentation userRepresentation, RealmResource realmResource, UsersResource usersResource, Response response) {
        RoleRepresentation userRole = realmResource.roles()
                .get(Role.USER.name())
                .toRepresentation();

        log.info("Assigning ROLE_USER in keycloak to user with username {}", userRepresentation.getUsername());
        usersResource.get(getKeycloakUserId(response))
                .roles()
                .realmLevel()
                .add(Collections.singletonList(userRole));
    }

    private UserRepresentation buildKeycloakUserRepresentation(CreateUserRequestDto createUserDto, String userId) {
        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setFirstName(createUserDto.firstName());
        userRepresentation.setLastName(createUserDto.lastName());
        userRepresentation.setEmailVerified(true);
        userRepresentation.setEnabled(true);
        userRepresentation.setCredentials(Collections.singletonList(buildCredentialRepresentation()));
        userRepresentation.setUsername(userId);
        return userRepresentation;
    }

    private CredentialRepresentation buildCredentialRepresentation() {
        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
        String password = RandomStringUtils.randomAlphabetic(8);
        credentialRepresentation.setValue(password);
        return credentialRepresentation;
    }

    private void validateCreateKeycloakUserResponse(Response response) {
        if (Response.Status.CREATED != response.getStatusInfo()) {
            throw new KeycloakUserCreationException();
        }
    }

    private String getKeycloakUserId(Response response) {
        return Optional.ofNullable(response.getLocation().getPath())
                .map(path -> path.substring(path.lastIndexOf("/") + 1))
                .orElseThrow(KeycloakUserCreationException::new);
    }
}
