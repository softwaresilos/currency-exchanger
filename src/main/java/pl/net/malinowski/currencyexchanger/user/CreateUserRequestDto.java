package pl.net.malinowski.currencyexchanger.user;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Builder;

import java.math.BigDecimal;

@Builder
record CreateUserRequestDto(@NotEmpty String firstName,
                            @NotEmpty String lastName,
                            @NotNull @Positive BigDecimal initialPlnBalance) {

}
