package pl.net.malinowski.currencyexchanger.security;

import lombok.NonNull;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthenticationHelper {

    public boolean isThisMyAccount(@NonNull String id) {
        return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication().getName())
                .map(id::equals)
                .orElse(false);
    }
}
