package pl.net.malinowski.currencyexchanger.error;

public class BadRequestException extends RuntimeException{

    public BadRequestException(String message) {
        super(message);
    }
}
