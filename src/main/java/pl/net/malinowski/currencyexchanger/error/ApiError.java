package pl.net.malinowski.currencyexchanger.error;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.Map;

@Builder
record ApiError(HttpStatus status,
                LocalDateTime date,
                String message,
                @JsonInclude(JsonInclude.Include.NON_NULL) Map<String, String> validationErrors) {

}
