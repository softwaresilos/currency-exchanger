package pl.net.malinowski.currencyexchanger.account.operation;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Builder;
import pl.net.malinowski.currencyexchanger.account.SupportedCurrency;

import java.math.BigDecimal;

@Builder
record AccountOperationRequestDto(@NotNull @Positive BigDecimal operationAmount,
                                  @NotNull SupportedCurrency sourceCurrency,
                                  @NotNull SupportedCurrency destinationCurrency) {
}
