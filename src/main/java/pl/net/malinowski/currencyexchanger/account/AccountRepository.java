package pl.net.malinowski.currencyexchanger.account;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.net.malinowski.currencyexchanger.user.UserEntity;

import java.util.List;

public interface AccountRepository extends JpaRepository<AccountEntity, Long> {

    List<AccountEntity> findByUser(UserEntity user);
}
