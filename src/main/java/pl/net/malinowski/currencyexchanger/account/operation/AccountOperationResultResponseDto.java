package pl.net.malinowski.currencyexchanger.account.operation;

import pl.net.malinowski.currencyexchanger.account.AccountDetailsDto;

import java.util.List;

record AccountOperationResultResponseDto(String id,
                                         List<AccountDetailsDto> accounts) {
}
