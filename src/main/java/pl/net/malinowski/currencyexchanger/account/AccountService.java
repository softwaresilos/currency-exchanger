package pl.net.malinowski.currencyexchanger.account;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.net.malinowski.currencyexchanger.user.UserEntity;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;

    public List<AccountEntity> createAccounts(UserEntity user, BigDecimal initialPlnBalance) {
        List<AccountEntity> accountsToCreate = Arrays.stream(SupportedCurrency.values())
                .map(currency -> buildAccountEntity(user, initialPlnBalance, currency))
                .toList();
        return accountRepository.saveAll(accountsToCreate);
    }

    private AccountEntity buildAccountEntity(UserEntity user, BigDecimal initialPlnBalance,
                                             SupportedCurrency currency) {
        AccountEntity.AccountEntityBuilder accountBuilder = AccountEntity.builder()
                .user(user)
                .currency(currency)
                .balance(BigDecimal.ZERO);
        if (SupportedCurrency.PLN == currency) {
            accountBuilder.balance(initialPlnBalance.setScale(2, RoundingMode.HALF_UP));
        }
        return accountBuilder.build();
    }
}
