package pl.net.malinowski.currencyexchanger.account.operation;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.net.malinowski.currencyexchanger.account.AccountEntity;
import pl.net.malinowski.currencyexchanger.account.AccountRepository;
import pl.net.malinowski.currencyexchanger.account.SupportedCurrency;
import pl.net.malinowski.currencyexchanger.exchangerate.ExchangeRateDto;
import pl.net.malinowski.currencyexchanger.exchangerate.ExchangeRateService;
import pl.net.malinowski.currencyexchanger.account.transaction.CurrencyTransactionEntity;
import pl.net.malinowski.currencyexchanger.account.transaction.CurrencyTransactionService;
import pl.net.malinowski.currencyexchanger.user.UserEntity;
import pl.net.malinowski.currencyexchanger.user.UserService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Service
@RequiredArgsConstructor
class AccountOperationService {

    private final ExchangeRateService exchangeRateService;
    private final UserService userService;
    private final AccountRepository accountRepository;
    private final CurrencyTransactionService currencyTransactionService;
    private final AccountOperationMapper accountOperationMapper;

    @Transactional
    public AccountOperationResultResponseDto exchange(String userId, AccountOperationRequestDto requestDto) {
        validateRequestData(requestDto);
        UserEntity user = userService.findUserById(userId);
        List<AccountEntity> accounts = accountRepository.findByUser(user);

        AccountEntity sourceAccount = getAccountByCurrency(accounts, userId, requestDto.sourceCurrency());
        AccountEntity destinationAccount = getAccountByCurrency(accounts, userId, requestDto.destinationCurrency());
        sourceAccount.checkIfFundsEnough(requestDto.operationAmount());

        ExchangeRateDto exchangeRate = getExchangeRateAmount(requestDto);
        makeExchange(requestDto, sourceAccount, destinationAccount, exchangeRate);
        createTransactionHistoryEntry(requestDto, sourceAccount, destinationAccount, exchangeRate);

        return accountOperationMapper.map(userId, accounts);
    }

    private void makeExchange(AccountOperationRequestDto requestDto, AccountEntity sourceAccount, AccountEntity destinationAccount, ExchangeRateDto exchangeRate) {
        if (SupportedCurrency.PLN == sourceAccount.getCurrency()) {
            BigDecimal calculatedRate = BigDecimal.ONE.divide(exchangeRate.buyingRate(), 4, RoundingMode.HALF_UP);
            sourceAccount.subtract(requestDto.operationAmount());
            BigDecimal operationResult = requestDto.operationAmount().multiply(calculatedRate);
            destinationAccount.add(operationResult);
        } else {
            sourceAccount.subtract(requestDto.operationAmount());
            destinationAccount.add(requestDto.operationAmount().multiply(exchangeRate.sellingRate()));
        }
    }

    private void createTransactionHistoryEntry(AccountOperationRequestDto requestDto, AccountEntity sourceAccount,
                                               AccountEntity destinationAccount, ExchangeRateDto exchangeRate) {
        CurrencyTransactionEntity transactionEntry = CurrencyTransactionEntity.builder()
                .transactionAmount(requestDto.operationAmount())
                .destinationAccount(destinationAccount)
                .sourceAccount(sourceAccount)
                .sellingExchangeRate(exchangeRate.sellingRate().setScale(4, RoundingMode.HALF_UP))
                .buyingExchangeRate(exchangeRate.buyingRate().setScale(4, RoundingMode.HALF_UP))
                .build();
        currencyTransactionService.createTransactionEntry(transactionEntry);
    }

    private ExchangeRateDto getExchangeRateAmount(AccountOperationRequestDto requestDto) {
        SupportedCurrency currencyToFetchRate = getCurrencyToFetchRate(requestDto);
        return exchangeRateService.fetchActualRateInPln(currencyToFetchRate);
    }

    private void validateRequestData(AccountOperationRequestDto requestDto) {
        if (requestDto.destinationCurrency() == requestDto.sourceCurrency()) {
            throw new AccountOperationFailedException("Destination currency cannot be the same as source currency");
        }
    }

    private SupportedCurrency getCurrencyToFetchRate(AccountOperationRequestDto requestDto) {
        if (SupportedCurrency.PLN != requestDto.sourceCurrency()) {
            return requestDto.sourceCurrency();
        }
        return requestDto.destinationCurrency();
    }

    private AccountEntity getAccountByCurrency(List<AccountEntity> accounts, String userId, SupportedCurrency currency) {
        return accounts.stream()
                .filter(account -> currency == account.getCurrency())
                .findFirst()
                .orElseThrow(() -> new AccountOperationFailedException(
                        String.format("Account with currency %s not found for user %s", currency, userId)));
    }
}
