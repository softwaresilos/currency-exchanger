package pl.net.malinowski.currencyexchanger.account.transaction.history;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/accounts/user/{userId}/transactions")
class AccountTransactionHistoryController {

    private final AccountTransactionHistoryService accountTransactionHistoryService;

    @GetMapping
    @PreAuthorize("@authenticationHelper.isThisMyAccount(#userId)")
    AccountTransactionHistoryResponseDto getAccountTransactions(@PathVariable String userId) {
        return accountTransactionHistoryService.getAccountTransactions(userId);
    }
}
