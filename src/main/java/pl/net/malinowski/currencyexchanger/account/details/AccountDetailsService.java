package pl.net.malinowski.currencyexchanger.account.details;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.net.malinowski.currencyexchanger.account.AccountEntity;
import pl.net.malinowski.currencyexchanger.account.AccountRepository;
import pl.net.malinowski.currencyexchanger.user.UserEntity;
import pl.net.malinowski.currencyexchanger.user.UserService;

import java.util.List;

@Service
@RequiredArgsConstructor
class AccountDetailsService {

    private final UserService userService;
    private final AccountDetailsMapper accountDetailsMapper;
    private final AccountRepository accountRepository;

    AccountsDetailsResponseDto findAccountsByUserId(String userId) {
        UserEntity user = userService.findUserById(userId);
        List<AccountEntity> userAccounts = accountRepository.findByUser(user);
        return accountDetailsMapper.mapToAccountsDetailsResponseDto(user, userAccounts);
    }
}
