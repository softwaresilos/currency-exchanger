package pl.net.malinowski.currencyexchanger.account.details;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import pl.net.malinowski.currencyexchanger.account.AccountEntity;
import pl.net.malinowski.currencyexchanger.configuration.MapStructConfig;
import pl.net.malinowski.currencyexchanger.user.UserEntity;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, config = MapStructConfig.class)
interface AccountDetailsMapper {

    @Mapping(source = "accounts", target = "accounts")
    AccountsDetailsResponseDto mapToAccountsDetailsResponseDto(UserEntity user, List<AccountEntity> accounts);
}
