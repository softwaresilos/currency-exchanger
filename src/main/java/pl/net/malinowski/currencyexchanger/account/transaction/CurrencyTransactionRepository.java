package pl.net.malinowski.currencyexchanger.account.transaction;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

interface CurrencyTransactionRepository extends JpaRepository<CurrencyTransactionEntity, UUID> {

    List<CurrencyTransactionEntity> findBySourceAccountId(Long sourceAccountId);
}
