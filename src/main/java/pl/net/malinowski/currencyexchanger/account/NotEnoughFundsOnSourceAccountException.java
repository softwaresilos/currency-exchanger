package pl.net.malinowski.currencyexchanger.account;

import pl.net.malinowski.currencyexchanger.error.BadRequestException;

public class NotEnoughFundsOnSourceAccountException extends BadRequestException {

    public NotEnoughFundsOnSourceAccountException(String message) {
        super(message);
    }
}
