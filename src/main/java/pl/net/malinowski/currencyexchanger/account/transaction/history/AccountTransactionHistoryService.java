package pl.net.malinowski.currencyexchanger.account.transaction.history;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.net.malinowski.currencyexchanger.account.AccountEntity;
import pl.net.malinowski.currencyexchanger.account.AccountRepository;
import pl.net.malinowski.currencyexchanger.account.transaction.CurrencyTransactionService;
import pl.net.malinowski.currencyexchanger.user.UserEntity;
import pl.net.malinowski.currencyexchanger.user.UserService;

import java.util.List;

@Service
@RequiredArgsConstructor
class AccountTransactionHistoryService {

    private final UserService userService;
    private final CurrencyTransactionService currencyTransactionService;
    private final AccountRepository accountRepository;
    private final AccountTransactionHistoryMapper accountTransactionHistoryMapper;

    AccountTransactionHistoryResponseDto getAccountTransactions(String userId) {
        UserEntity user = userService.findUserById(userId);
        List<AccountEntity> accounts = accountRepository.findByUser(user);
        return accountTransactionHistoryMapper
                .map(userId, currencyTransactionService.getTransactionsForAccounts(accounts));
    }
}
