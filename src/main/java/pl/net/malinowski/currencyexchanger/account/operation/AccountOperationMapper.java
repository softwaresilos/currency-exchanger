package pl.net.malinowski.currencyexchanger.account.operation;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import pl.net.malinowski.currencyexchanger.account.AccountEntity;
import pl.net.malinowski.currencyexchanger.configuration.MapStructConfig;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, config = MapStructConfig.class)
interface AccountOperationMapper {

    @Mapping(source = "id", target = "id")
    @Mapping(source = "accounts", target = "accounts")
    AccountOperationResultResponseDto map(String id, List<AccountEntity> accounts);
}
