package pl.net.malinowski.currencyexchanger.account.details;

import pl.net.malinowski.currencyexchanger.account.AccountDetailsDto;

import java.util.List;

record AccountsDetailsResponseDto(String id,
                                  String firstName,
                                  String lastName,
                                  List<AccountDetailsDto> accounts) {
}
