package pl.net.malinowski.currencyexchanger.account;

import java.math.BigDecimal;

public record AccountDetailsDto(Long id,
                         SupportedCurrency currency,
                         BigDecimal balance) {
}
