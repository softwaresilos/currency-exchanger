package pl.net.malinowski.currencyexchanger.account.transaction.history;

import pl.net.malinowski.currencyexchanger.account.SupportedCurrency;

import java.math.BigDecimal;
import java.time.LocalDateTime;

record AccountTransactionEntryDto(String id,
                                  BigDecimal amount,
                                  SupportedCurrency sourceCurrency,
                                  SupportedCurrency destinationCurrency,
                                  LocalDateTime createdDate) {
}
