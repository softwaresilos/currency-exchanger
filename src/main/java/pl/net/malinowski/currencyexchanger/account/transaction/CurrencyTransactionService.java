package pl.net.malinowski.currencyexchanger.account.transaction;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.net.malinowski.currencyexchanger.account.AccountEntity;

import java.util.Collection;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CurrencyTransactionService {

    private final CurrencyTransactionRepository currencyTransactionRepository;

    public void createTransactionEntry(CurrencyTransactionEntity transaction) {
        currencyTransactionRepository.save(transaction);
    }

    public List<CurrencyTransactionEntity> getTransactionsForAccounts(List<AccountEntity> accounts) {
        return accounts.stream()
                .map(account -> currencyTransactionRepository.findBySourceAccountId(account.getId()))
                .flatMap(Collection::stream)
                .toList();
    }
}
