package pl.net.malinowski.currencyexchanger.account;

public enum SupportedCurrency {
    PLN,
    USD
}
