package pl.net.malinowski.currencyexchanger.account.operation;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/accounts/user/{userId}")
class AccountOperationController {

    private final AccountOperationService accountOperationService;

    @PutMapping("/exchange")
    @PreAuthorize("@authenticationHelper.isThisMyAccount(#userId)")
    AccountOperationResultResponseDto exchange(@PathVariable String userId,
                                               @Valid @RequestBody AccountOperationRequestDto requestDto) {
        return accountOperationService.exchange(userId, requestDto);
    }

}
