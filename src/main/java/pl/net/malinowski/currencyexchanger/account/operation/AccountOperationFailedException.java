package pl.net.malinowski.currencyexchanger.account.operation;

import pl.net.malinowski.currencyexchanger.error.BadRequestException;

class AccountOperationFailedException extends BadRequestException {

    public AccountOperationFailedException(String message) {
        super(message);
    }
}
