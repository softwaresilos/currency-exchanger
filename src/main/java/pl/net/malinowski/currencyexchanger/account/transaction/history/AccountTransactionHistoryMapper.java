package pl.net.malinowski.currencyexchanger.account.transaction.history;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import pl.net.malinowski.currencyexchanger.account.transaction.CurrencyTransactionEntity;
import pl.net.malinowski.currencyexchanger.configuration.MapStructConfig;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, config = MapStructConfig.class)
interface AccountTransactionHistoryMapper {

    @Mapping(source = "id", target = "id")
    @Mapping(source = "transactions", target = "transactions")
    AccountTransactionHistoryResponseDto map(String id, List<CurrencyTransactionEntity> transactions);

    @Mapping(source = "sourceAccount.currency", target = "sourceCurrency")
    @Mapping(source = "destinationAccount.currency", target = "destinationCurrency")
    @Mapping(source = "transactionAmount", target = "amount")
    AccountTransactionEntryDto mapTransactionEntry(CurrencyTransactionEntity transaction);
}
