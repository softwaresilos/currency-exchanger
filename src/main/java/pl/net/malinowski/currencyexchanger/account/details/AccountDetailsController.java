package pl.net.malinowski.currencyexchanger.account.details;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/accounts")
class AccountDetailsController {

    private final AccountDetailsService accountDetailsService;

    @GetMapping("/user/{userId}")
    @PreAuthorize("@authenticationHelper.isThisMyAccount(#userId)")
    AccountsDetailsResponseDto getAccountsForUser(@PathVariable String userId) {

        return accountDetailsService.findAccountsByUserId(userId);
    }

}
