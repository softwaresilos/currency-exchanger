package pl.net.malinowski.currencyexchanger.account.transaction.history;

import java.util.List;

record AccountTransactionHistoryResponseDto(String id,
                                            List<AccountTransactionEntryDto> transactions) {
}
