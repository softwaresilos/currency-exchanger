package pl.net.malinowski.currencyexchanger.account;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.net.malinowski.currencyexchanger.user.UserEntity;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Data
@Entity
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "accounts")
public class AccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private BigDecimal balance = BigDecimal.ZERO;

    @Enumerated(value = EnumType.STRING)
    private SupportedCurrency currency;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity user;

    public void subtract(BigDecimal operationAmount) {
        checkIfFundsEnough(operationAmount);
        balance = balance.subtract(operationAmount).setScale(2, RoundingMode.HALF_UP);
    }

    public void add(BigDecimal operationAmount) {
        balance = balance.add(operationAmount).setScale(2, RoundingMode.HALF_UP);
    }

    public void checkIfFundsEnough(BigDecimal operationAmount) {
        if (balance.compareTo(operationAmount) < 0) {
            throw new NotEnoughFundsOnSourceAccountException(String.format("Not enough funds on source account in %s for user %s",
                    currency, user.getId()));
        }
    }
}
