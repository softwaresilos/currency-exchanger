package pl.net.malinowski.currencyexchanger.exchangerate;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.net.malinowski.currencyexchanger.account.SupportedCurrency;

@FeignClient(value = "nbpApiClient",
        url = "${currency-exchanger.exchange-rate.api.url}")
interface NbpExchangeRateApiClient {

    @GetMapping("/exchangerates/rates/c/{currency}")
    NbpApiResponse getExchangeRate(@PathVariable SupportedCurrency currency);
}
