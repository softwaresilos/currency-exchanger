package pl.net.malinowski.currencyexchanger.exchangerate;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.net.malinowski.currencyexchanger.account.SupportedCurrency;

@Slf4j
@Service
@RequiredArgsConstructor
public class ExchangeRateService {

    private final NbpExchangeRateApiClient nbpExchangeRateApiClient;

    public ExchangeRateDto fetchActualRateInPln(SupportedCurrency currency) {
        log.info("Fetching actual exchange rate in PLN for {}", currency);
        NbpApiResponse exchangeRate = nbpExchangeRateApiClient.getExchangeRate(currency);
        NbpApiRate rate = exchangeRate.rates().get(0);
        log.info("Fetched actual exchange rate in PLN {} for USD", rate);
        return ExchangeRateDto.builder()
                .currency(SupportedCurrency.valueOf(exchangeRate.code()))
                .buyingRate(rate.bid())
                .sellingRate(rate.ask())
                .build();
    }
}
