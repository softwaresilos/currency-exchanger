package pl.net.malinowski.currencyexchanger.exchangerate;

import feign.Response;
import feign.codec.ErrorDecoder;
import pl.net.malinowski.currencyexchanger.error.BadRequestException;
import pl.net.malinowski.currencyexchanger.error.NotFoundException;

import javax.naming.ServiceUnavailableException;

public class FeignNbpErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String methodKey, Response response) {
        return switch (response.status()) {
            case 400 -> new BadRequestException(String.format("NBP Api: %s", response.reason()));
            case 404 -> new NotFoundException(String.format("NBP Api: %s", response.reason()));
            case 503 -> new ServiceUnavailableException("NBP Api is unavailable");
            default -> new Exception("Exception while getting NPB rate details");
        };
    }
}
