package pl.net.malinowski.currencyexchanger.exchangerate;

import lombok.Builder;
import pl.net.malinowski.currencyexchanger.account.SupportedCurrency;

import java.math.BigDecimal;

@Builder
public record ExchangeRateDto(BigDecimal sellingRate,
                              BigDecimal buyingRate,
                              SupportedCurrency currency) {
}
