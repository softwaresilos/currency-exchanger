package pl.net.malinowski.currencyexchanger.exchangerate;

import lombok.Builder;

import java.util.List;

@Builder
record NbpApiResponse(String currency,
                      String code,
                      List<NbpApiRate> rates) {
}
