package pl.net.malinowski.currencyexchanger.exchangerate;

import lombok.Builder;

import java.math.BigDecimal;
import java.time.LocalDate;

@Builder
public record NbpApiRate(String no,
                         LocalDate effectiveDate,
                         BigDecimal bid,
                         BigDecimal ask) {
}
